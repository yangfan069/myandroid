package com.sys.android.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBTools extends SQLiteOpenHelper{
	
	private  static final  String DATABASE_NAME = "chat_db";
	private  static final  int DATABASE_VERSION = 1;
	private static DBTools  dbTools = null;
	public DBTools(Context context){
		
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
		dbTools = new DBTools(context);
		dbTools.getWritableDatabase();
		System.out.println("创建数据表应该进行中。。。。。");
	}
	//创建数据库
	@Override
	public void onCreate(SQLiteDatabase db) {
		 System.out.println("开始创建数据表。。。");
		String sql = "create table if not exists chat "
				+"(_id integer primary key autoincrement, type integer , body text , from text , to text)";
		db.execSQL(sql);
		
	}
	//更新数据库
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		
	}
	
	public static DBTools getInstance(Context context){
		
		if(dbTools == null){
			dbTools = new DBTools(context);
			System.out.println("获取到了对象");
		}
		return dbTools;
	}
	
}
