package com.sys.android.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.util.Base64;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sys.android.entity.ChatMsg;
import com.sys.android.util.MyApplication;
import com.sys.android.util.RecordButton;
import com.sys.android.util.RecordButton.OnFinishedRecordListener;
import com.sys.android.util.Utils;
import com.sys.android.xmpp.R;
import com.sys.android.xmppmanager.XmppConnection;

@SuppressLint("HandlerLeak")
public class ChatActivity extends Activity {
	
	ChatManager cm = null;
	Chat newchat = null;
	//获取录音按钮
	private RecordButton mRecordButton = null;
	private MediaPlayer mediaPlayer = null;
    private String PhotoPath = null;
	private String userChat="";//当前聊天
	private MyAdapter adapter;
	private List<ChatMsg> listMsg = new ArrayList<ChatMsg>();
	private String pUSERID;//当前登录用户
	private String pFRIENDID;//聊天用户
	private EditText msgText;
	private TextView chat_name;
    private NotificationManager mNotificationManager;
    private int IMAGE_CODE = 1;
    Bitmap bitmap = null;
    String time = null;
    private String InPhotoPath = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.chat_client);
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);		
		//获取Intent传过来的用户名
		this.pUSERID = getIntent().getStringExtra("USERID");
		this.userChat=getIntent().getStringExtra("user");
		this.pFRIENDID = getIntent().getStringExtra("FRIENDID");
		MyApplication.getInstance().addActivity(this);
		System.out.println("接收消息的用户是："+pFRIENDID + " ; " + "发送消息的用户是："+pUSERID);
		chat_name = (TextView)findViewById(R.id.chat_name);
		chat_name.setText(userChat);
		
		ListView listview = (ListView) findViewById(R.id.formclient_listview);
		
		listview.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		
		this.adapter = new MyAdapter(this);
		
		listview.setAdapter(adapter);
		
		
/*		FileTransferManager ftmGet = new FileTransferManager(XmppConnection.getConnection());
		ftmGet.addFileTransferListener(new FileTransferListener(){

			@Override
			public void fileTransferRequest(FileTransferRequest request) {
				Toast.makeText(getApplicationContext(), "正在接收文件", 0).show(); 
				IncomingFileTransfer accept = request.accept(); 
				File file = new File("/mnt/sdcard/" + request.getFileName()); 

				try { 
					accept.recieveFile(file);
					
					Toast.makeText(getApplicationContext(), "接收成功!", 0).show(); 
				} catch (XMPPException e) { 
					Toast.makeText(getApplicationContext(), "接收失败!", 
					Toast.LENGTH_SHORT).show(); 
					e.printStackTrace(); 
				}

			}
			
		});*/
		
		//获取文本信息
		this.msgText = (EditText) findViewById(R.id.formclient_text);
		class OnBTClickListener implements View.OnClickListener {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.chat_back:
				{
					finish();
					break;
				}
				case R.id.formclient_btsend:
				{
					//获取text文本
					String msg = msgText.getText().toString();			
					if(msg.length() > 0){
						listMsg.add(new ChatMsg("OUT", msg, pUSERID, userChat,0));
						adapter.notifyDataSetChanged();				
						try {
							Message Stringmsg = new Message();
							Stringmsg.setLanguage("font");
							Stringmsg.setSubject(msg);
							newchat.sendMessage(Stringmsg);	
						}catch (XMPPException e){
							e.printStackTrace();
						}
					}
					else
					{
						Toast.makeText(ChatActivity.this, "发送信息不能为空", Toast.LENGTH_SHORT).show();
					}
					msgText.setText("");
					break;
				}
				case R.id.send_picture:
				{
					try {
					/*	
						FileTransferManager ftm = new FileTransferManager(XmppConnection.getConnection());
						//ftm.addFileTransferListener(arg0)
						OutgoingFileTransfer fileTransfer = ftm.createOutgoingFileTransfer(userChat);
						
						File picFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/abc.jpg");
						fileTransfer.sendFile(picFile, "Send");
						
						ChatMsg args = new ChatMsg("IN", "收到文件！" + picFile.toString(), userChat, pUSERID, 0);
						android.os.Message msg = handler.obtainMessage();
						msg.what = 1;
						msg.obj = args;
						msg.sendToTarget();*/
						
						//要发送的图片的路径
						Intent intentPhoto = new Intent();
						intentPhoto.setType("image/*");
						intentPhoto.setAction(Intent.ACTION_GET_CONTENT);
						startActivityForResult(intentPhoto,IMAGE_CODE);
						System.out.println("跳入选择图片的界面");
					} catch (Exception e) {
						System.out.println("ToPic or Send Err:" + e.toString());
					}
				}
				default:
					break;
					
				}
			}
		}
		OnBTClickListener lis = new OnBTClickListener();

		//返回按钮
		Button mBtnBack = (Button) findViewById(R.id.chat_back);
		mBtnBack.setOnClickListener(lis);
		
		//发送消息
		Button btnSendText = (Button)findViewById(R.id.formclient_btsend);
		btnSendText.setOnClickListener(lis);
		
		//图片按钮
	    Button btnPicture = (Button)findViewById(R.id.send_picture);
	    btnPicture.setOnClickListener(lis);
		//录音保存的路径
	   
		mRecordButton = (RecordButton) findViewById(R.id.btnRecordSend);
		mRecordButton.setOnFinishedRecordListener(
				new OnFinishedRecordListener() {
					@Override
					public void onFinishedRecord(String audioPath) {

						System.out.println("我录的音  结果保存在 ： " + audioPath);
						//从路径里面拿出来文件转换为String 发送给对方
						//文件转换
						try {
							System.out.println("将路径" +audioPath+"的录音转换为STRING." );
							String recordString = Utils.encodeBase64File(audioPath);
							//发送的是语音
							listMsg.add(new ChatMsg("OUT", recordString, pUSERID, userChat,1));	
							System.out.println("已将录音" + recordString + " 发送过去。");
							adapter.notifyDataSetChanged();	
							Message recordMessage = new Message();
							recordMessage.setBody(recordString);
							recordMessage.setLanguage("voice");
							recordMessage.setSubject(recordString);
							newchat.sendMessage(recordMessage);
						} catch (Exception e) {
							
							System.out.println("转换失败！");
							
							e.printStackTrace();
						}
					}
				});
	}
	
	private String GetPath() {
		Long l = System.currentTimeMillis();
		String fileName = l.toString();
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName + ".amr";
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		//消息监听
		cm = XmppConnection.getConnection().getChatManager();
		//创建当前用户的会话
		newchat = cm.createChat(pFRIENDID, new msgListener());
		//发送消息给pc服务器的好友（获取自己的服务器，和好友）
		cm.addChatListener(new ChatManagerListener() {
			@Override
			public void chatCreated(Chat newchat, boolean able) {
				newchat.addMessageListener(new msgListener());
			}
		});
	}

	
	class msgListener implements MessageListener{
		String pPath = null;
		@Override
		public void processMessage(Chat chat, Message message) {
			//收到来自pc服务器的消息（获取自己好友发来的信息）
			if(message.getFrom().contains(userChat) && "font".equals(message.getLanguage()))
				{
				    ChatMsg args = new ChatMsg("IN", message.getSubject(), userChat, pUSERID, 0);
					android.os.Message msg = handler.obtainMessage();
					msg.what = 1;
					msg.obj = args;
					msg.sendToTarget();
				}else if(message.getFrom().contains(userChat) && "voice".equals(message.getLanguage())){
					ChatMsg voice = new ChatMsg("IN", message.getSubject(), userChat, pUSERID, 1);
				
					android.os.Message msg = handler.obtainMessage();
					msg.what = 0;
					msg.obj = voice;
					msg.sendToTarget();
				}else if(message.getFrom().contains(userChat) && "photo".equals(message.getLanguage())){
                    
					ChatMsg photo = new ChatMsg("IN", message.getSubject(), userChat, pUSERID, 2);
					android.os.Message msg = handler.obtainMessage();
					msg.what = 2;
					msg.obj = photo;
					msg.sendToTarget();
				}else{
					System.out.println("不明类型的文件！@。。");
				}
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 super.onActivityResult(requestCode, resultCode, data);
	    ContentResolver resolver = this.getContentResolver();
		if(requestCode == IMAGE_CODE && data != null){
			
			try {
				Uri originalUri = data.getData();
				bitmap = MediaStore.Images.Media.getBitmap(resolver, originalUri);
				bitmap.toString();
			    String[] proj = {MediaStore.Images.Media.DATA};      
				Cursor cursor = managedQuery(originalUri, proj, null, null, null); 
	            //按我个人理解 这个是获得用户选择的图片的索引值
	            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);  
	            cursor.moveToFirst();
	            //最后根据索引值获取图片路径
	            PhotoPath = cursor.getString(column_index);
	    		android.os.Message msg = handler.obtainMessage();
				msg.what = 3;
				msg.obj = PhotoPath;
				msg.sendToTarget();
	            //将路径PhotoPath下的文件转换为Base64码
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
	   
	}
    //oncreate方法终止
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			
			switch (msg.what) {
			case 0:
				ChatMsg voice = (ChatMsg) msg.obj;
				listMsg.add(voice);
				adapter.notifyDataSetChanged();
				break;
			case 1:
				ChatMsg args = (ChatMsg) msg.obj;
				listMsg.add(args);
				adapter.notifyDataSetChanged();
				break;		
			case 2:
				ChatMsg photo = (ChatMsg) msg.obj;
				String picString = photo.getMsg();
				InPhotoPath = GetPicPath();
				File file = new File(InPhotoPath);
                 if(!file.exists()){
						try {
							file.createNewFile();
							byte[] buffer = Base64.decode(picString);
							FileOutputStream out = new FileOutputStream(InPhotoPath);
							out.write(buffer);
							out.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
				listMsg.add(photo);
				adapter.notifyDataSetChanged();
				break;
			case 3:
				String photoPath = (String)msg.obj;
				try {
					 String picBase64 = Utils.encodeBase64File(photoPath);
					 System.out.println("将路径"+PhotoPath+"下的文件转换为Base64码");
			         ChatMsg pct = new ChatMsg("OUT",  picBase64, pUSERID, userChat,2);
			         listMsg.add(pct);
			         adapter.notifyDataSetChanged();
			         Message photoMsg = new Message();
			         photoMsg.setLanguage("photo");
			         photoMsg.setSubject(picBase64);
			         newchat.sendMessage(photoMsg);
				} catch (Exception e) {
					e.printStackTrace();
				}
		           break;
			default:
				break;
			}
		};
	};
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//按返回键需要处理的问题
	}
	//获取随机的图片路径
	public String GetPicPath() {
		Long l = System.currentTimeMillis();
		String fileName = l.toString();
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName+ ".jpg";
	}
	class MyAdapter extends BaseAdapter {
 
	
		private Context cxt;
		private LayoutInflater inflater;
		
		
		public MyAdapter(ChatActivity formClient) {
			this.cxt = formClient;
		}
		
		@Override
		public int getCount() {
		
			return listMsg.size();
		}
		@Override
		public Object getItem(int position) {
			return listMsg.get(position);
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			this.inflater = (LayoutInflater) this.cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			int state = listMsg.get(position).getState();
			switch(state){
			
			case 0:
				if(listMsg.get(position).getType().equals("IN")){
					convertView = this.inflater.inflate(R.layout.formclient_chat_in, null);
					TextView msgView = (TextView) convertView.findViewById(R.id.formclient_row_msg);
					msgView.setText(listMsg.get(position).getMsg());
				}else{
					convertView = this.inflater.inflate(R.layout.formclient_chat_out, null);
					TextView msgView = (TextView) convertView.findViewById(R.id.formclient_row_msg);
					msgView.setText(listMsg.get(position).getMsg());
				}
				break;
			case 1:
				String VoiceString = listMsg.get(position).getMsg();
				if(listMsg.get(position).getType().equals("IN")){
					convertView = this.inflater.inflate(R.layout.friendclient_chat_voice_in, null);
				}else{
					convertView = this.inflater.inflate(R.layout.friendclient_chat_voice_out, null);
				}
				Button button = (Button)convertView.findViewById(R.id.formclient_row_voice_in);
				try {
					final String path = GetPath();
					Utils.decoderBase64File(VoiceString, path);
					System.out.println("我将接收到的录音字符串： " + VoiceString +"存放到" + path +"路径底下");
					button.setOnClickListener(new OnClickListener(){
						@Override
						public void onClick(View v) {
							mediaPlayer = new MediaPlayer();
							Toast.makeText(getBaseContext(), "开始播放录音",Toast.LENGTH_SHORT).show();
							try {
								mediaPlayer.setDataSource(path);
								System.out.println("我接受到的播放的录音路径是： " + path);
								mediaPlayer.prepare();
							} catch (Exception e) {
								e.printStackTrace();
							}
							mediaPlayer.start();
						}
						
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 2:
				if(listMsg.get(position).getType().equals("IN")){
					convertView = this.inflater.inflate(R.layout.friendclient_chat_photo_in, null);
					Bitmap Inbitmap = BitmapFactory.decodeFile(InPhotoPath);
					ImageView imageView = (ImageView)convertView.findViewById(R.id.formclient_row_photo_in);
					imageView.setImageBitmap(Inbitmap);
					 break;
				}else{
					convertView = this.inflater.inflate(R.layout.friendclient_chat_photo_out, null);
					   System.out.println("我的图片路径为：" + PhotoPath + "我是发送出去的图片");
					   bitmap = BitmapFactory.decodeFile(PhotoPath);
					   ImageView imageView = (ImageView)convertView.findViewById(R.id.formclient_row_photo_out);
					   imageView.setImageBitmap(bitmap);
					break;
					
				}
			   default:
			        break;
			}	 	
			return convertView;
		}
	}	

	protected void setNotiType(int iconId, String title, String msg) {
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent appIntent = PendingIntent.getActivity(this, 0, intent, 0);
		Notification myNoti = new Notification();
		myNoti.icon = iconId;
		myNoti.tickerText = msg;
		myNoti.defaults = Notification.DEFAULT_SOUND;
		myNoti.flags |= Notification.FLAG_AUTO_CANCEL;
		myNoti.setLatestEventInfo(this, title, msg, appIntent);
		mNotificationManager.notify(0, myNoti);
	}
}