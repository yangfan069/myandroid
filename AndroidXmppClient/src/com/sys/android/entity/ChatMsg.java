package com.sys.android.entity;

import java.io.Serializable;

import com.sys.android.xmppmanager.XmppConnection;

import android.text.TextUtils;

@SuppressWarnings("serial")
public class ChatMsg implements Serializable{ 
	
	/**
	 * 
	 */
	private String type;
	private String msg;
	private String from;
	private String to;
	private int state;
	
	
	public ChatMsg() {
	
	}


	public ChatMsg(String type, String msg, String from, String to, int state) {
	
		this.type = type;
		this.msg = msg;
		this.from = from;
		this.to = to;
		this.state = state;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public String getFrom() {
		return from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getTo() {
		return to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public int getState() {
		return state;
	}


	public void setState(int state) {
		this.state = state;
	}


	public String getJid(){
		if(!TextUtils.isEmpty(from)){
			return from + "@" +XmppConnection.SERVER_NAME;
		}
		return from;
	}


	@Override
	public String toString() {
		return type+"|" + msg +"|" + from+ "|" + to + "|"+ "state";
	}
	
}
